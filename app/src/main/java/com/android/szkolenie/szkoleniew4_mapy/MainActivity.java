package com.android.szkolenie.szkoleniew4_mapy;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private MapView mMapa;
    private EditText mWyszukiwarka;
    private Button mZnajdz;

    private GoogleMap mMapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMapa = (MapView) findViewById(R.id.map_view);
        mWyszukiwarka = (EditText) findViewById(R.id.wyszukiwarka);
        mZnajdz = (Button) findViewById(R.id.trasa);

        mMapa.onCreate(savedInstanceState);
        mMapController = mMapa.getMap();
        MapsInitializer.initialize(this);   // Inicjalizacja mapy
        if(mMapController != null) {
            mMapController.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMapController.setMyLocationEnabled(true);  // Wyświetl lokalizacje telefonu
            // W momencie znalezienie lokalizacji telefonu chcemy przyblizyc mape do naszego punktu
            mMapController.setOnMyLocationChangeListener(
                    new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    // Dostaliśmy nasza bieżącą lokalizacje.
                    // Przyblizamy mape !
                    if(location != null) {
                        mMapController.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(location.getLatitude(), location.getLongitude()),
                                19
                        ));
                        // Narysowanie linni od pozycji uzytkownika do pinezki
                        // o wspolrzednych 51.12, 17.01
                        mMapController.addPolyline(new PolylineOptions()
                        .add(new LatLng(location.getLatitude(), location.getLongitude()))
                        .add(new LatLng(51.12, 17.01)));
                        mMapController.setOnMyLocationChangeListener(null);
                    }
                }
            });

            // Obsluga przycisku Znajdz
            mZnajdz.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wyszukajTrase();
                }
            });

            // Narysujmy kilka punktów
            Marker mMarker = mMapController.addMarker(new MarkerOptions()
                    .draggable(true) // Czy mozna przesuwać ten marker ?
                    .position(new LatLng(51.12, 17.01))
                    .title("Tytuł")
                    .snippet("Podtytuł")
                    .icon(BitmapDescriptorFactory.defaultMarker(46))    // 46 to H ze skali HSV,
                    // natomist S i V maja wartosci 100
            );
        } else {
            Toast.makeText(this, "Błąd ładowania mapy !", Toast.LENGTH_SHORT).show();
        }
    }

    // Wyszukanie punktu na podstawie podanego adresu
    // Narysowanie trasy do zznalezionego punktu
    protected void wyszukajTrase() {
        Geocoder mGeoCoder = new Geocoder(this);
        try {
            List<Address> mResults = mGeoCoder.getFromLocationName(mWyszukiwarka.getText().toString(), 1);
            if(mResults != null && mResults.size() > 0) {
                Toast.makeText(this,
                        mResults.get(0).getLatitude() + ", "+mResults.get(0).getLongitude(), Toast.LENGTH_SHORT).show();
                Location myLocation = mMapController.getMyLocation();
                if(myLocation != null) {
                    new DirectionsAsyncTask().execute(
                            new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                            new LatLng(mResults.get(0).getLatitude(), mResults.get(0).getLongitude()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class DirectionsAsyncTask extends AsyncTask<LatLng, Void, ArrayList<LatLng>> {

        @Override
        protected ArrayList<LatLng> doInBackground(LatLng... latLngs) {
            GMapV2Direction mDirections = new GMapV2Direction();
            Document dc = mDirections.getDocument(
                    latLngs[0],
                    latLngs[1],
                    GMapV2Direction.MODE_DRIVING);
            ArrayList<LatLng> directions = mDirections.getDirection(dc);
            return directions;

        }

        @Override
        protected void onPostExecute(ArrayList<LatLng> latLngs) {
            super.onPostExecute(latLngs);
            mMapController.addPolyline(new PolylineOptions().addAll(latLngs));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapa.onResume();
    }
}
